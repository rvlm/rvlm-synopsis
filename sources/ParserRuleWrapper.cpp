#include "rvlm/synopsis/ParserRuleWrapper.h"
#include "rvlm/synopsis/ArgumentRule.h"
#include "rvlm/synopsis/ArgumentsRule.h"
#include "rvlm/synopsis/ProgramNameRule.h"
#include "rvlm/synopsis/OptionRule.h"
#include "rvlm/SwitchRule.h"

#define WRAP(ClassName, args) \
    ParserRuleWrapper(new impl:: ClassName##Rule args)

namespace rvlm {
namespace synopsis {

namespace {

std::list<std::wstring> LIST(
    const wchar_t* arg1,
    const wchar_t* arg2,
    const wchar_t* arg3,
    const wchar_t* arg4)
{
    std::list<std::wstring> result;
    if (arg1) result.push_back(arg1);
    if (arg2) result.push_back(arg2);
    if (arg3) result.push_back(arg3);
    if (arg4) result.push_back(arg4);
    return result;
}

} // namespace

// --- ProgramName ---

ParserRuleWrapper ProgramName(std::wstring* target) {
    return WRAP(ProgramName, (target));
}

// --- Argument ---

ParserRuleWrapper Argument(std::wstring* target) {
    return WRAP(Argument, (target));
}

ParserRuleWrapper Argument(int* target) {
    return WRAP(Argument, (target));
}

ParserRuleWrapper Argument(unsigned* target) {
    return WRAP(Argument, (target));
}

ParserRuleWrapper Argument(float* target) {
    return WRAP(Argument, (target));
}

ParserRuleWrapper Argument(double* target) {
    return WRAP(Argument, (target));
}

// --- Arguments ---

ParserRuleWrapper Arguments(std::vector<std::wstring> *target) {
    return WRAP(Arguments, (target));
}

// --- Switch ---

ParserRuleWrapper Switch(bool *target,
    const std::list<std::wstring>& argNames)
{
    return WRAP(Switch, (target, argNames));
}

ParserRuleWrapper Switch(int* count,
    const std::list<std::wstring>& argNames)
{
    return WRAP(Switch, (count, argNames));
}

ParserRuleWrapper Switch(bool* target, int* count,
    const std::list<std::wstring>& argNames)
{
    return WRAP(Switch, (target, count, argNames));
}

ParserRuleWrapper Switch(bool *target,
    const wchar_t* a1, const wchar_t* a2, const wchar_t* a3,const wchar_t* a4)
{
    return WRAP(Switch, (target, LIST(a1,a2,a3,a4)));
}

ParserRuleWrapper Switch(int* count,
    const wchar_t* a1, const wchar_t* a2, const wchar_t* a3,const wchar_t* a4)
{
    return WRAP(Switch, (count, LIST(a1,a2,a3,a4)));
}

ParserRuleWrapper Switch(bool* target, int* count,
    const wchar_t* a1, const wchar_t* a2, const wchar_t* a3,const wchar_t* a4)
{
    return WRAP(Switch, (target, count, LIST(a1,a2,a3,a4)));
}

// --- Option ---

ParserRuleWrapper Option(
    std::wstring* target, const std::list<std::wstring>& argNames)
{
    return WRAP(Option, (target, argNames));
}

ParserRuleWrapper Option(
    int *target, const std::list<std::wstring>& argNames)
{
    return WRAP(Option, (target, argNames));
}

ParserRuleWrapper Option(
    unsigned *target, const std::list<std::wstring>& argNames)
{
    return WRAP(Option, (target, argNames));
}

ParserRuleWrapper Option(
    float *target, const std::list<std::wstring>& argNames)
{
    return WRAP(Option, (target, argNames));
}

ParserRuleWrapper Option(
    double *target, const std::list<std::wstring>& argNames)
{
    return WRAP(Option, (target, argNames));
}

ParserRuleWrapper Option(std::wstring* target,
    const wchar_t* a1,
    const wchar_t* a2,
    const wchar_t* a3,
    const wchar_t* a4)
{
    return WRAP(Option, (target, LIST(a1,a2,a3,a4)));
}

ParserRuleWrapper Option(int* target,
    const wchar_t* a1,
    const wchar_t* a2,
    const wchar_t* a3,
    const wchar_t* a4)
{
    return WRAP(Option, (target, LIST(a1,a2,a3,a4)));
}

ParserRuleWrapper Option(unsigned* target,
    const wchar_t* a1,
    const wchar_t* a2,
    const wchar_t* a3,
    const wchar_t* a4)
{
    return WRAP(Option, (target, LIST(a1,a2,a3,a4)));
}

ParserRuleWrapper Option(float* target,
    const wchar_t* a1,
    const wchar_t* a2,
    const wchar_t* a3,
    const wchar_t* a4)
{
    return WRAP(Option, (target, LIST(a1,a2,a3,a4)));
}

ParserRuleWrapper Option(double* target,
    const wchar_t* a1,
    const wchar_t* a2,
    const wchar_t* a3,
    const wchar_t* a4)
{
    return WRAP(Option, (target, LIST(a1,a2,a3,a4)));
}

} // namespace synopsis
} // namespace rvlm
