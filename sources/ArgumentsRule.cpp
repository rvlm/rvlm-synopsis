#include "rvlm/synopsis/ArgumentsRule.h"

namespace rvlm {
namespace synopsis {

ArgumentsRule::ArgumentsRule(
    std::vector<std::wstring> *target)
{
    mTarget = target;
}

void ArgumentsRule::reset()
{
    setCanTakeArgument(true);
}

bool ArgumentsRule::feed(
    const std::wstring& optArg)
{
    mTarget->push_back(optArg);
    return true;
}

} // namespace synopsis
} // namespace rvlm
