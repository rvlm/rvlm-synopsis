#include "rvlm/synopsis/ArgumentRule.h"

namespace rvlm { 
namespace synopsis {

ArgumentRule::ArgumentRule(std::wstring *target) {
    setTarget(target); }

ArgumentRule::ArgumentRule(int *target) {
    setTarget(target); }

ArgumentRule::ArgumentRule(unsigned int *target) {
    setTarget(target); }

ArgumentRule::ArgumentRule(float *target) {
    setTarget(target); }

ArgumentRule::ArgumentRule(double *target) {
    setTarget(target); }

void ArgumentRule::reset()
{
    setCanTakeArgument(true);
}

bool ArgumentRule::feed(const std::wstring& optArg)
{
    bool okay = setTargetValue(optArg);
    if (okay)
        setCanTakeArgument(false);

    return okay;
}

} // namespace synopsis
} // namespace rvlm
