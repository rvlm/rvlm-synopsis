#include <algorithm>
#include <functional>
#include "rvlm/synopsis/ParserRule.h"
#include "rvlm/synopsis/ParserRules.h"

namespace rvlm {
namespace synopsis {

namespace {
namespace internal {

/**
 * @internal
 * Auxiliary method which had to be added for successful compilation in '-ansi' mode.
 * In this mode GCC literally follows ANSI standard descriptions -- even buggy ones.
 * In ANSI mode @c std::bind1st and @std::bind2nd are not able to bind argument passed
 * to function by constant reference (<tt>const T&</tt>). Also, ANSI mode lacks useful
 * @c std::binary_compose extension which would allow to just write:
 * @code
 *    return findRule(
 *        std::bind2nd(
 *            std::binary_compose(
 *                std::mem_fun(&impl::ParserRule::knowsLongOption),
 *                std::identity,
 *                std::derefence),
 *            &longOption));
 *    );
 * @endcode
 * or something like that, because I'm unsure @c std::dereference exists.
 */
bool wrapKnowsLongOption(
    const impl::ParserRule* thisptr,
    const std::wstring* optptr)
{
    return thisptr->knowsLongOption(*optptr);
}

} // namespace internal
} // namespace

ParserRules::ParserRules()
{
}

ParserRules::~ParserRules()
{
    iterator iter = begin();
    for ( ; iter != end(); ++iter)
        delete *iter;
}

impl::ParserRule* ParserRules::findWhoCanTakeProgramName() const
{
    return findRule(
        std::mem_fun(&impl::ParserRule::canTakeProgramName));
}

impl::ParserRule* ParserRules::findWhoCanTakeArgument() const
{
    return findRule(
        std::mem_fun(&impl::ParserRule::canTakeArgument));
}

impl::ParserRule* ParserRules::findWhoKnows(wchar_t shortOption) const
{
    return findRule(
        std::bind2nd(
            std::mem_fun(&impl::ParserRule::knowsShortOption),
            shortOption));
}

impl::ParserRule* ParserRules::findWhoKnows(
    const std::wstring& longOption) const
{
    return findRule(
        std::bind2nd(
            std::ptr_fun(&internal::wrapKnowsLongOption),
            &longOption));
}

} // namespace synopsis
} // namespace rvlm
