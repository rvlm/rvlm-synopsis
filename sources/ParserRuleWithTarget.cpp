#include "rvlm/synopsis/ParserRuleWithTarget.h"
#include "rvlm/Text/Parse.h"

namespace rvlm {
namespace synopsis {

// Here are used some parsing functions from another library, effectively
// causing a dependency. Say thnx to C++ standard library designers who didn't
// add str2int or alike function.
using Fdtd::Text::parse;

void ParserRuleWithTarget::setTarget(std::wstring *target)
{
    targetType = TARGET_WSTRING;
    targetPtr = target;
}

void ParserRuleWithTarget::setTarget(int *target)
{
    targetType = TARGET_INT;
    targetPtr = target;
}

void ParserRuleWithTarget::setTarget(unsigned int *target)
{
    targetType = TARGET_UINT;
    targetPtr = target;
}

void ParserRuleWithTarget::setTarget(float *target)
{
    targetType = TARGET_FLOAT;
    targetPtr = target;
}

void ParserRuleWithTarget::setTarget(double *target)
{
    targetType = TARGET_DOUBLE;
    targetPtr = target;
}

bool ParserRuleWithTarget::setTargetValue(const std::wstring& optArg)
    throw(std::logic_error)
{
    if (!targetPtr)
        return false;

    switch (targetType)
    {
    case TARGET_WSTRING: *(std::wstring*)targetPtr = optArg; return true;
    case TARGET_INT:     return parse((int*)targetPtr, optArg);
    case TARGET_UINT:    return parse((unsigned*)targetPtr, optArg);
    case TARGET_FLOAT:   return parse((float*)targetPtr, optArg);
    case TARGET_DOUBLE:  return parse((double*)targetPtr, optArg);
    default:
        throw std::logic_error(
            "Wrong target type detected (check library source code).");
    }
}

} // namespace synopsis
} // namespace rvlm
