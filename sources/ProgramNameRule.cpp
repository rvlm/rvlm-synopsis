#include "rvlm/synopsis/ProgramNameRule.h"

namespace rvlm {
namespace synopsis {

ProgramNameRule::ProgramNameRule(std::wstring *target)
{
    mTarget = target;
    setCanTakeProgramName(true);
}

void ProgramNameRule::reset()
{
    setCanTakeProgramName(true);
}

bool ProgramNameRule::feed(const std::wstring& optArg)
{
    mTarget->assign(optArg);
    return true;
}

} // namespace synopsis
} // namespace rvlm
