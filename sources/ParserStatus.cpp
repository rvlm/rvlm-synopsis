#include "rvlm/synopsis/ParserStatus.h"

namespace rvlm {
namespace synopsis {

ParserStatus::ParserStatus()
{
    mErrorType = NO_ERROR;
}

ParserStatus::ParserStatus(ErrorType type)
{
    mErrorType = type;
}

ParserStatus::ParserStatus(
    ErrorType type, const std::wstring& longOption)
{
    mErrorType = type;
    mOptionName = L"--" + longOption;
}

ParserStatus::ParserStatus(ErrorType type, wchar_t shortOption)
{
    mErrorType = type;
    mOptionName = L"-" + shortOption;
}

} // namespace synopsis
} // namespace rvlm
