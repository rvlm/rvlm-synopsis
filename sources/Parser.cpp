#include <algorithm>
#include <functional>
#include "rvlm/text/Encodings.h"
#include "rvlm/synopsis/Parser.h"
#include "rvlm/synopsis/ParserRule.h"

namespace rvlm {
namespace synopsis {

std::list<std::wstring> convertArgsFromLocal(int argc, const char* argv[])
{
    std::list<std::wstring> result;
    for (int i=0; i<argc; ++i)
        result.push_back(Fdtd::Text::convertFromLocal(argv[i]));

    return result;
}

ParserStatus parseCommandLine(
    const std::list<std::wstring>& argvs,
    ParserRules& rules)
{
    // Revert any state after any possible previous command line parsing
    // pass to reuse existing rules. It is stated as perfectly legal in
    // 'ParserRule' class documentation.
    ParserRules::iterator ruleIter = rules.begin();
    for ( ; ruleIter != rules.end(); ++ruleIter)
    {
        ParserRule* rule = *ruleIter;
        if (rule)
            rule->reset();
    }

    bool noMoreOptions = false;
    std::list<std::wstring>::const_iterator argIter = argvs.begin();
    for ( ; argIter != argvs.end() ; ++argIter)
    {
        std::wstring curArg = *argIter;

        if (argIter == argvs.begin())
        {
            // This is the first argument which holds program name.
            ParserRule *rule = rules.findWhoCanTakeProgramName();
            if (rule)
                rule->feed(curArg);
        }
        else if (curArg.length() == 2 && curArg[0] == L'-' && curArg[1] == L'-')
        {
            // This is special double dash '--' option which denotes the end
            // of all options. The rest must be treated as arguments.
            noMoreOptions = true;
        }
        else if (!noMoreOptions &&
            curArg.length() > 2 && curArg[0] == L'-' && curArg[1] == L'-')
        {
            // This is long option '--something'. Now find rule which can
            // recognize it.
            std::wstring longOption;
            std::wstring optArg;

            size_t eqsignPos = curArg.find(L'=', 1);
            if (eqsignPos == std::wstring::npos)
                longOption = curArg.substr(2);
            else
            {
                longOption = curArg.substr(2, eqsignPos-2);
                optArg = curArg.substr(eqsignPos+1);
            }

            ParserRule *rule = rules.findWhoKnows(longOption);
            if (!rule)
            {
                return ParserStatus(
                    ParserStatus::UNRECOGNIZED_LONG_OPTION,
                    longOption);
            }

            if (eqsignPos != std::wstring::npos && !rule->requiresArgument())
            {
                return ParserStatus(
                    ParserStatus::EXTRA_ARG_FOR_LONG_OPTION,
                    longOption);
            }

            if (eqsignPos == std::wstring::npos && rule->requiresArgument())
            {
                ++argIter;
                if (argIter == argvs.end())
                {
                    return ParserStatus(
                        ParserStatus::MISSING_ARG_FOR_LONG_OPTION,
                        longOption);
                }

                optArg = *argIter;
            }

            bool result = rule->feed(optArg);
            if (!result)
            {
                return ParserStatus(
                    ParserStatus::UNRECOGNIZED_LONG_OPTION,
                    longOption);
            }
        }
        else if (!noMoreOptions &&
            curArg.length() > 1 && curArg[0] == L'-')
        {
            // This is a bunch of short options '-abcd'. Omit the dash and
            // find rule for each option given.
            for (size_t idx = 1; idx < curArg.length(); ++idx)
            {
                wchar_t shortOption = curArg[idx];
                ParserRule *rule = rules.findWhoKnows(shortOption);

                if (!rule)
                {
                    return ParserStatus(
                        ParserStatus::UNRECOGNIZED_SHORT_OPTION,
                        shortOption);
                }

                std::wstring optArg;
                if (rule->requiresArgument())
                {
                    if (idx < curArg.length() - 1)
                    {
                        int startCutIdx = idx + 1;
                        if (curArg[startCutIdx] == L'=')
                            ++startCutIdx;

                        // Stop if option requires argument. The rest of
                        // bunch is treated as its argument.
                        idx = curArg.length();

                        optArg = curArg.substr(startCutIdx);
                    }
                    else
                    {
                        ++argIter;
                        if (argIter != argvs.end())
                        {
                            return ParserStatus(
                                ParserStatus::MISSING_ARG_FOR_SHORT_OPTION,
                                shortOption);
                        }

                        optArg = *argIter;
                    }
                }

                rule->feed(optArg);
            }
        }
        else
        {
            // If none above matched, its an argument.
            ParserRule *rule = rules.findWhoCanTakeArgument();
            if (rule)
                rule->feed(curArg);
        }
    }

    return ParserStatus(ParserStatus::NO_ERROR);
}

} // namespace synopsis
} // namespace rvlm
