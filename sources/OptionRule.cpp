#include "rvlm/synopsis/OptionRule.h"

namespace rvlm {
namespace synopsis {

OptionRule::OptionRule(std::wstring *target,
    const std::list<std::wstring>& argNames)
{
    setTarget(target);
    addKnownOptions(argNames);
}

OptionRule::OptionRule(int *target,
    const std::list<std::wstring>& argNames)
{
    setTarget(target);
    addKnownOptions(argNames);
}

OptionRule::OptionRule(unsigned int *target,
    const std::list<std::wstring>& argNames)
{
    setTarget(target);
    addKnownOptions(argNames);
}

OptionRule::OptionRule(float *target,
    const std::list<std::wstring>& argNames)
{
    setTarget(target);
    addKnownOptions(argNames);
}

OptionRule::OptionRule(double *target,
    const std::list<std::wstring>& argNames)
{
    setTarget(target);
    addKnownOptions(argNames);
}

void OptionRule::reset()
{
    setRequiresArgument(true);
}

bool OptionRule::feed(const std::wstring& optarg)
{
    return setTargetValue(optarg);
}

} // namespace synopsis
} // namespace rvlm
