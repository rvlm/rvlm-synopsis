#include "rvlm/synopsis/SwitchRule.h"

namespace rvlm {
namespace synopsis {

SwitchRule::SwitchRule(bool *target,
    const std::list<std::wstring>& argNames)
{
    mTarget = target;
    mCount  = NULL;
    addKnownOptions(argNames);
}

SwitchRule::SwitchRule(int* count,
    const std::list<std::wstring>& argNames)
{
    mTarget = NULL;
    mCount  = count;
    addKnownOptions(argNames);
}

SwitchRule::SwitchRule(bool* target, int* count,
    const std::list<std::wstring>& argNames)
{
    mTarget = target;
    mCount  = count;
    addKnownOptions(argNames);
}

void SwitchRule::reset()
{
    if (mTarget) *mTarget = false;
    if (mCount)  *mCount  = 0;
}

bool SwitchRule::feed(const std::wstring&)
{
    if (mTarget) *mTarget = true;
    if (mCount)  ++*mCount;
    return true;
}

} // namespace synopsis
} // namespace rvlm
