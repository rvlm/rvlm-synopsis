#include "rvlm/synopsis/ParserRule.h"
#include <algorithm>

namespace rvlm {
namespace synopsis {

ParserRule::ParserRule()
{
    mRequiresArgument = false;
    mCanTakeArgument  = false;
    mCanTakeProgramName = false;
}

bool ParserRule::knowsShortOption(wchar_t opt) const
{
    return std::find(
        mKnownShortOptions.begin(),
        mKnownShortOptions.end(),
        opt) != mKnownShortOptions.end();
}

bool ParserRule::knowsLongOption(const std::wstring& opt) const
{
    return std::find(
        mKnownLongOptions.begin(),
        mKnownLongOptions.end(),
        opt) != mKnownLongOptions.end();
}

void ParserRule::addKnownOptions(const std::list<std::wstring>& opts)
{
    std::list<std::wstring>::const_iterator iter;
    for (iter = opts.begin(); iter != opts.end(); ++iter)
    {
        addKnownOption(*iter);
    }
}

void ParserRule::addKnownOption(const std::wstring& opt)
{
    if (opt.length() > 2 && opt[0] == L'-' && opt[1] == L'-')
        mKnownLongOptions.push_back(opt.substr(2));
    else if (opt.length() > 1 && opt[0] == L'-')
        mKnownShortOptions.push_back(opt[1]);
}

void ParserRule::reset()
{
}

bool ParserRule::feed(const std::wstring& optArg)
{
    (void)optArg;
    return false;
}

} // namespace synopsis
} // namespace rvlm
