#pragma once
#include <string>

namespace rvlm {
namespace synopsis {

class ParserStatus
{
public:

    enum ErrorType
    {
        NO_ERROR,
        UNRECOGNIZED_LONG_OPTION,
        UNRECOGNIZED_SHORT_OPTION,
        EXTRA_ARG_FOR_LONG_OPTION,
        MISSING_ARG_FOR_LONG_OPTION,
        MISSING_ARG_FOR_SHORT_OPTION
    };

    ParserStatus();

    ParserStatus(ErrorType type);

    ParserStatus(ErrorType type, const std::wstring& longOption);

    ParserStatus(ErrorType type, wchar_t shortOption);

    ErrorType errorType() const { return mErrorType; }

    bool success() const { return errorType() == NO_ERROR; }

    const std::wstring& optionName() const { return mOptionName; }

private:

    ErrorType mErrorType;
    std::wstring mOptionName;
};

} // namespace synopsis
} // namespace rvlm
