#pragma once
#include <auto_ptr.h>
#include <string>
#include <list>
#include <vector>

namespace rvlm {
namespace synopsis {

class ParserRule;

typedef std::auto_ptr<impl::ParserRule> ParserRuleWrapper;

ParserRuleWrapper ProgramName(std::wstring* target);

/// @{
ParserRuleWrapper Argument(std::wstring* target);
ParserRuleWrapper Argument(int* target);
ParserRuleWrapper Argument(unsigned* target);
ParserRuleWrapper Argument(float* target);
ParserRuleWrapper Argument(double* target);
/// @}

ParserRuleWrapper Arguments(std::vector<std::wstring> *target);

/// @{
ParserRuleWrapper Switch(bool *target,
    const std::list<std::wstring>& argNames);

ParserRuleWrapper Switch(int* count,
    const std::list<std::wstring>& argNames);

ParserRuleWrapper Switch(bool* target, int* count,
    const std::list<std::wstring>& argNames);
/// @}

/// @{
ParserRuleWrapper Switch(bool *target,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);

ParserRuleWrapper Switch(int* count,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);

ParserRuleWrapper Switch(bool* target, int* count,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);
/// @}

/// @{
ParserRuleWrapper Option(std::wstring* target,
    const std::list<std::wstring>& argNames);

ParserRuleWrapper Option(int* target,
    const std::list<std::wstring>& argNames);

ParserRuleWrapper Option(unsigned* target,
    const std::list<std::wstring>& argNames);

ParserRuleWrapper Option(float* target,
    const std::list<std::wstring>& argNames);

ParserRuleWrapper Option(double* target,
    const std::list<std::wstring>& argNames);
/// @}

/// @{
ParserRuleWrapper Option(std::wstring* target,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);

ParserRuleWrapper Option(int* target,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);

ParserRuleWrapper Option(unsigned* target,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);

ParserRuleWrapper Option(float* target,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);

ParserRuleWrapper Option(double* target,
    const wchar_t* a1,
    const wchar_t* a2 = NULL,
    const wchar_t* a3 = NULL,
    const wchar_t* a4 = NULL);
/// @}

} // namespace synopsis
} // namespace rvlm
