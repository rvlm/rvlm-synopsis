#pragma once
#include <list>
#include <string>

namespace rvlm {
namespace synopsis {

class ParserRule
{
public:

    ParserRule();

    ~ParserRule() {}

    bool canTakeProgramName() const { return mCanTakeProgramName; }

    bool canTakeArgument() const { return mCanTakeArgument; }

    bool requiresArgument() const { return mRequiresArgument; }

    bool knowsShortOption(wchar_t opt) const;

    bool knowsLongOption(const std::wstring& opt) const;

    virtual void reset() = 0;

    virtual bool feed(const std::wstring& optarg) = 0;

protected:

    void setCanTakeProgramName(bool val) { mCanTakeProgramName = val; }

    void setCanTakeArgument(bool val) { mCanTakeArgument = val; }

    void setRequiresArgument(bool val) { mRequiresArgument = val; }

    void addKnownOptions(const std::list<std::wstring>& opts);

    void addKnownOption(const std::wstring& opt);

private:

    std::list<std::wstring> mKnownLongOptions;
    std::list<wchar_t> mKnownShortOptions;
    bool mCanTakeProgramName;
    bool mCanTakeArgument;
    bool mRequiresArgument;
};

} // namespace synopsis
} // namespace fdtd
