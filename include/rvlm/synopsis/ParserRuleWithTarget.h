#pragma once
#include <stdexcept>
#include "rvlm/synopsis/ParserRule.h"

namespace rvlm {
namespace synopsis {

class ParserRuleWithTarget: public ParserRule
{
protected:

    void setTarget(std::wstring *target);
    void setTarget(int *target);
    void setTarget(unsigned int *target);
    void setTarget(float *target);
    void setTarget(double *target);

    bool setTargetValue(const std::wstring& optArg) throw(std::logic_error);

private:

    enum TargetType
    {
        TARGET_WSTRING,
        TARGET_INT,
        TARGET_UINT,
        TARGET_FLOAT,
        TARGET_DOUBLE
    };

    TargetType targetType;

    void *targetPtr;
};

} // namespace synopsis
} // namespace rvlm
