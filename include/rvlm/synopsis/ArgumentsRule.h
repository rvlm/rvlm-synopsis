#pragma once
#include <vector>
#include "rvlm/synopsis/ParserRule.h"

namespace rvlm {
namespace synopsis {

class ArgumentsRule: public ParserRule
{
public:

    ArgumentsRule(std::vector<std::wstring> *target);

    virtual void reset();

    virtual bool feed(const std::wstring& optArg);

private:

    std::vector<std::wstring> *mTarget;
};

} // namespace synopsis
} // namespace rvlm
