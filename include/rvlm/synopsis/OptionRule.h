#pragma once
#include "rvlm/synopsis/ParserRuleWithTarget.h"

namespace rvlm {
namespace synopsis {

class OptionRule: public ParserRuleWithTarget
{
public:

    OptionRule(std::wstring *target, const std::list<std::wstring>& argNames);

    OptionRule(int *target, const std::list<std::wstring>& argNames);

    OptionRule(unsigned int *target, const std::list<std::wstring>& argNames);

    OptionRule(float *target, const std::list<std::wstring>& argNames);

    OptionRule(double *target, const std::list<std::wstring>& argNames);

    OptionRule(std::wstring *target,
        const wchar_t* argName1 = NULL,
        const wchar_t* argName2 = NULL,
        const wchar_t* argName3 = NULL,
        const wchar_t* argName4 = NULL,
        const wchar_t* argName5 = NULL);

    OptionRule(int *target,
        const wchar_t* argName1 = NULL,
        const wchar_t* argName2 = NULL,
        const wchar_t* argName3 = NULL,
        const wchar_t* argName4 = NULL,
        const wchar_t* argName5 = NULL);

    OptionRule(unsigned int *target,
        const wchar_t* argName1 = NULL,
        const wchar_t* argName2 = NULL,
        const wchar_t* argName3 = NULL,
        const wchar_t* argName4 = NULL,
        const wchar_t* argName5 = NULL);

    OptionRule(float *target,
        const wchar_t* argName1 = NULL,
        const wchar_t* argName2 = NULL,
        const wchar_t* argName3 = NULL,
        const wchar_t* argName4 = NULL,
        const wchar_t* argName5 = NULL);

    OptionRule(double *target,
        const wchar_t* argName1 = NULL,
        const wchar_t* argName2 = NULL,
        const wchar_t* argName3 = NULL,
        const wchar_t* argName4 = NULL,
        const wchar_t* argName5 = NULL);

    virtual void reset();

    virtual bool feed(const std::wstring& optArg);
};

} // namespace synopsis
} // namespace rvlm
