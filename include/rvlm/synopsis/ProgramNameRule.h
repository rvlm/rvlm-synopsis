#pragma once
#include "rvlm/synopsis/ParserRule.h"

namespace rvlm {
namespace synopsis {

class ProgramNameRule: public ParserRule
{
public:

    ProgramNameRule(std::wstring *target);

    virtual void reset();

    virtual bool feed(const std::wstring& optArg);

private:

    std::wstring *mTarget;
};

} // namespace synopsis
} // namespace rvlm
