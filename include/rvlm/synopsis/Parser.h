#pragma once
#include <list>
#include <string>
#include "rvlm/synopsis/ParserRules.h"
#include "rvlm/synopsis/ParserStatus.h"

namespace rvlm {
namespace synopsis {

std::list<std::wstring> convertArgsFromLocal(int argc, const char* argv[]);

/**
 * @code
 * std::list<std::wstring> args = convertArgsFromLocal(argc, argv);
 * ParserStatus status = ParseCommandLine(args,
 * ParserRules()
 *     << ProgramName()
 *     << Switch()
 *     << Option()
 *     << Argument()
 *     << Argument()
 *     << Arguments()
 * @endcode
 */
ParserStatus parseCommandLine(
    const std::list<std::wstring>& argvs,
    ParserRules& rules);

} // namespace synopsis
} // namespace rvlm
