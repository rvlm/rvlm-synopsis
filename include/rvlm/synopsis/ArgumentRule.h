#pragma once
#include "rvlm/synopsis/ParserRuleWithTarget.h"

namespace fdtd {
namespace synopsis {

class ArgumentRule: public ParserRuleWithTarget
{
public:

    ArgumentRule(std::wstring *target);
    ArgumentRule(int *target);
    ArgumentRule(unsigned int *target);
    ArgumentRule(float *target);
    ArgumentRule(double *target);

    virtual void reset();

    virtual bool feed(const std::wstring& optArg);
};

} // namespace synopsis
} // namespace rvlm
