#pragma once
#include "rvlm/synopsis/ParserRule.h"

namespace rvlm {
namespace synopsis {

class SwitchRule: public ParserRule
{
public:

    SwitchRule(bool *target,
        const std::list<std::wstring>& argNames);

    SwitchRule(int* count,
        const std::list<std::wstring>& argNames);

    SwitchRule(bool* target, int* count,
        const std::list<std::wstring>& argNames);

    virtual void reset();

    virtual bool feed(const std::wstring& optArg);

private:

    bool *mTarget;
    int  *mCount;
};

} // namespace synopsis
} // namespace rvlm
