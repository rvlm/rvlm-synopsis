#pragma once
#include <list>
#include <string>
#include "rvlm/synopsis/ParserRuleWrapper.h"
#include "rvlm/Synopsis/ParserStatus.h"

namespace rvlm {
namespace synopsis {

class ParserRule;

class ParserRules: private std::list<ParserRule*>
{
public:

    ParserRules();

    ~ParserRules();

    ParserRules& operator << (ParserRuleWrapper ruleWrapper)
    {
        this->push_back(ruleWrapper.release());
        return *this;
    }

private:

    template <typename TPredicate>
    ParserRule* findRule(TPredicate predicate) const
    {
        ParserRules::const_iterator iter = begin();
        for ( ; iter != end(); ++iter) {
            ParserRule* rule = *iter;
            if (predicate(rule))
                return rule;
        }
        return NULL;
    }

    ParserRule* findWhoCanTakeProgramName() const;
    ParserRule* findWhoCanTakeArgument() const;
    ParserRule* findWhoKnows(wchar_t shortOption) const;
    ParserRule* findWhoKnows(const std::wstring& longOption) const;

friend ParserStatus parseCommandLine(
    const std::list<std::wstring>& argvs,
    ParserRules& rules);
};

} // namespace synopsis
} // namespace rvlm
