include_directories(include)
AddLibrary(Fdtdrvlm/synopsis
    include/rvlm/synopsis/Parser.h
    include/rvlm/synopsis/ParserStatus.h
    include/rvlm/synopsis/ParserRules.h
    include/rvlm/synopsis/ParserRuleWrapper.h
    include/rvlm/synopsis/ParserRule.h
    include/rvlm/synopsis/SwitchRule.h
    include/rvlm/synopsis/ProgramNameRule.h
    include/rvlm/synopsis/OptionRule.h
    include/rvlm/synopsis/ArgumentsRule.h
    include/rvlm/synopsis/ArgumentRule.h
    include/rvlm/synopsis/ParserRuleWithTarget.h
    sources/Parser.cpp
    sources/ParserRules.cpp
    sources/ParserStatus.cpp
    sources/ParserRule.cpp
    sources/SwitchRule.cpp
    sources/ProgramNameRule.cpp
    sources/OptionRule.cpp
    sources/ArgumentsRule.cpp
    sources/ArgumentRule.cpp
    sources/ParserRuleWithTarget.cpp
    sources/ParserRuleWrapper.cpp)

TargetLinkLibraries(Fdtdrvlm/synopsis FdtdText FdtdCore)
